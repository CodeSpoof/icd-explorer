import glob
import json
import math
import os
import re
import threading
import time
from typing import Any, Dict, List

import qdarkgraystyle as qdarkgraystyle
import requests
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QFont, QKeySequence, QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QInputDialog, QShortcut, QTabBar, QTreeView
from threading import Lock

from gui import Ui_MainWindow
import auth
from popup import Ui_Dialog

loaders = []

class onlineTreeView(QTreeView):
    searching = False
    workers = []
    readLock = Lock()
    writeLock = Lock()
    wolist = []
    wodict = {}
    ind = 0
    
    def requer(self):
        item = ""
        with self.readLock:
            item = self.wolist[self.ind]
            self.ind += 1
        try:
            n_dat = request_json(item, gl.token)
            with self.writeLock:
                self.wodict[item] = n_dat
        except:
            pass
    
    def searchItems(self, searchterm):
        self.searching = True
        self.parent().searching = True
        self.setCursor(Qt.WaitCursor)
        self.model.clear()
        search_data = request_json(search_url(self.url, {"q": searchterm, "flatResults": "true"}), gl.token)
        dat = []
        for result in search_data["destinationEntities"]:
            dat.append(result["id"])
        self.wolist = dat
        self.wodict.clear()
        self.workers.clear()
        self.ind = 0
        for i in range(0, len(self.wolist)):
            self.workers.append(threading.Thread(target=self.requer, args=()))
            self.workers[i].start()
        while len(self.wodict) < len(self.wolist):
            time.sleep(0.1)
        for subchild in dat:
            n_dat = self.wodict[subchild]
            n = StandardItem(n_dat['title']['@value'], 12, data=n_dat)
            if "child" in n_dat:
                n.appendRow(StandardItem())
            self.model.invisibleRootItem().appendRow(n)
        self.unsetCursor()
    
    def regularItems(self):
        self.searching = False
        self.parent().searching = False
        self.model.clear()
        n_dat = request_json(self.url, gl.token)
        n = StandardItem(n_dat['title']['@value'], 12, data=n_dat)
        self.model.invisibleRootItem().appendRow(n)
        ui.centralwidget.setCursor(Qt.WaitCursor)
        self.load(self.model.invisibleRootItem().child(0, 0))
        ui.centralwidget.unsetCursor()
    
    def load(self, child):
        self.setCursor(Qt.WaitCursor)
        try:
            dat = child.data(QtCore.Qt.UserRole)
            if not ("called" in dat and dat["called"]):
                try:
                    child = self.model.itemFromIndex(child)
                except Exception:
                    pass
                child.removeRows(0, child.rowCount())
                dat["called"] = True
                child.setData(dat, QtCore.Qt.UserRole)
                if "child" in dat:
                    self.wolist = dat["child"]
                    self.wodict.clear()
                    self.workers.clear()
                    self.ind = 0
                    for i in range(0, len(self.wolist)):
                        self.workers.append(threading.Thread(target=self.requer, args=()))
                        self.workers[i].start()
                    while len(self.wodict) < len(self.wolist):
                        time.sleep(0.1)
                    for subchild in dat["child"]:
                        n_dat = self.wodict[subchild]
                        n = StandardItem(n_dat['title']['@value'], 12, data=n_dat)
                        if "child" in n_dat:
                            n.appendRow(StandardItem())
                        child.appendRow(n)
        except KeyboardInterrupt:
            pass
        self.unsetCursor()

    def __init__(self, url):
        super().__init__()
        self.url = url
        self.expanded.connect(self.load)
        self.model = QStandardItemModel()
        self.setModel(self.model)
        n_dat = request_json(url, gl.token)
        matches = split_link(url)
        self.searchable = (matches["revision"] == "11")
        self.title = f"ICD {matches['revision']} {(matches['linearization'] + ' ') if matches['linearization'] is not None else ''}Release {matches['release']}"
        n = StandardItem(n_dat['title']['@value'], 12, data=n_dat)
        self.model.invisibleRootItem().appendRow(n)
        ui.centralwidget.setCursor(Qt.WaitCursor)
        self.load(self.model.invisibleRootItem().child(0, 0))
        ui.centralwidget.unsetCursor()
        self.setSelectionMode(self.SingleSelection)
        self.doubleClicked.connect(popup)
        self.setHeaderHidden(True)


class StandardItem(QStandardItem):
    
    def __init__(self, txt='', font_size=12, set_bold=False, color=QColor(255, 255, 255), data=None):
        super().__init__()
        
        if data is None:
            data = {}
        fnt = QFont('Open Sans', font_size)
        
        fnt.setBold(set_bold)
        
        self.setEditable(False)
        
        self.setForeground(color)
        
        self.setFont(fnt)
        
        self.setText(txt)
        
        self.setData(data, QtCore.Qt.UserRole)


class g:
    token = None
    startTime = None
    max = 1
    progress = 0
    end_workers = False
    loaded_dict = None
    models: Dict[str, QStandardItemModel] = {}


class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(ApplicationWindow, self).__init__()
        
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)


def linearization_url():
    return f"https://id.who.int/icd/release/{ui.dropRevision.currentText().split('/')[0]}/{ui.dropRelease.currentText()}{('/' + ui.dropRevision.currentText().split('/')[1]) if len(ui.dropRevision.currentText().split('/')) > 1 else ''}"


def search_url(lurl, data: dict):
    params = []
    for key in data:
        params.append(f"{key}={data[key]}")
    return lurl + "/search?" + "&".join(params)


def download():
    gl.startTime = time.time()
    loaders.append(Loader(linearization_url()))
    loaders[-1].statusUpdateHook.connect(updatehook)
    loaders[-1].statusFinished.connect(finishedLoader)
    loaders[-1].start()
    ui.buttonDownload.setDisabled(True)
    ui.dropRelease.setDisabled(True)
    ui.dropRevision.setDisabled(True)


def updatehook():
    ui.progress.setTextVisible(True)
    ui.progress.setMaximum(gl.max)
    ui.progress.setValue(gl.progress)


def finishedLoader():
    print(f"Download: {str(math.trunc((time.time() - gl.startTime) / 60))}:{str((time.time() - gl.startTime) % 60)}")
    gl.startTime = time.time()
    matches = split_link(list(gl.loaded_dict.keys())[0])
    print(matches)
    for key in ["revision", "release", "linearization"]:
        if key not in matches:
            matches[key] = "null"
    json.dump({"root": list(gl.loaded_dict.keys())[0], "data": gl.loaded_dict}, open(os.path.join("downloads", f"data_icd_{matches['revision']}_{matches['release']}{('_' + matches['linearization']) if matches['linearization'] is not None else ''}.json"), "w+"), indent=4)
    print(f"File: {str(math.trunc((time.time() - gl.startTime) / 60))}:{str((time.time() - gl.startTime) % 60)}")
    gl.startTime = time.time()
    addTreeTab({"root": list(gl.loaded_dict.keys())[0], "data": gl.loaded_dict})
    ui.tabs.tabBar().tabButton(ui.tabs.count() - 1, QTabBar.RightSide).resize(0, 0)
    ui.buttonDownload.setEnabled(True)
    ui.dropRelease.setEnabled(True)
    ui.dropRevision.setEnabled(True)
    print(f"Display: {str(math.trunc((time.time() - gl.startTime) / 60))}:{str((time.time() - gl.startTime) % 60)}")


def split_link(url: str) -> Dict[str, str]:
    return re.search(
            "https?://id.who.int/icd/release/(?P<revision>[0-9]{2})/(?P<release>[^/]*)(/(?P<linearization>.*))?",
            url).groupdict()


def worker(loader):
    _token = gl.token
    over = True
    while not gl.end_workers:
        url = ""
        with loader.index_lock:
            try:
                url = loader.working_list[loader.index]
                loader.index += 1
            except IndexError:
                over = False
        if over:
            json = request_json(url, _token)
            with loader.finished_count_lock:
                loader.working_dict[url] = json
                if "child" in json:
                    for child in json["child"]:
                        loader.working_list.append(child)
                loader.finished_count += 1
        else:
            over = True


def loadReleases():
    token = getToken(auth.id, auth.secret)
    ui.dropRelease.clear()
    ui.dropRelease.repaint()
    for release in request_json("https://id.who.int/icd/release/" + ui.dropRevision.currentText(), token)["release"]:
        ui.dropRelease.addItem(split_link(release)["release"])


def getToken(clientID, clientSecret) -> str:
    return requests.post('https://icdaccessmanagement.who.int/connect/token',
                         data={'client_id':  clientID, 'client_secret': clientSecret, 'scope': 'icdapi_access',
                               'grant_type': 'client_credentials'}).json()['access_token']


def request_json(link_: str, token_: str):
    headers_ = {
        'Authorization':   'Bearer ' + token_,
        'Accept':          'application/json',
        'Accept-Language': 'en',
        'API-Version':     'v2'
    }
    return requests.get(link_, headers=headers_).json()


def treeItem(key: str, d: Dict[str, Any]):
    dat = d[key]
    node = StandardItem(dat['title']['@value'], 12, data=dat)
    if "child" in node.data(QtCore.Qt.UserRole):
        for child in node.data(QtCore.Qt.UserRole)['child']:
            try:
                node.appendRow(treeItem(child, d))
            except:
                pass
    return node


def addTreeTab(d: Dict[str, Any]):
    Custom = QtWidgets.QWidget()
    Custom.setObjectName("Custom")
    verticalLayout = QtWidgets.QVBoxLayout(Custom)
    verticalLayout.setContentsMargins(0, 0, 0, 0)
    verticalLayout.setObjectName("verticalLayout")
    tree = QtWidgets.QTreeView(Custom)
    tree.setObjectName("tree")
    verticalLayout.addWidget(tree)
    matches = split_link(d["root"])
    if matches:
        title = f"ICD {matches['revision']} {(matches['linearization'] + ' ') if matches['linearization'] is not None else ''}Release {matches['release']}"
        gl.models[title] = QStandardItemModel()
        tree.setModel(gl.models[title])
        tree.setSelectionMode(tree.SingleSelection)
        tree.doubleClicked.connect(popup)
        tree.setHeaderHidden(True)
        root = gl.models[title].invisibleRootItem()
        root.appendRow(treeItem(d["root"], d["data"]))
        tree.searchable = False
        Custom.searchable = False
        ui.tabs.addTab(Custom, title)


def closeReq(index):
    ui.tabs.removeTab(index)


def openViewTab():
    custom = QtWidgets.QWidget()
    vertical_layout = QtWidgets.QVBoxLayout(custom)
    vertical_layout.setContentsMargins(0, 0, 0, 0)
    tree = onlineTreeView(linearization_url())
    vertical_layout.addWidget(tree)
    custom.searchable = tree.searchable
    custom.searching = tree.searching
    custom.searchItems = tree.searchItems
    custom.regularItems = tree.regularItems
    ui.tabs.addTab(custom, tree.title)


def popup(val):
    dialog = Ui_Dialog(val.data(QtCore.Qt.UserRole))
    dialog.setStyleSheet(qdarkgraystyle.load_stylesheet())
    dialog.exec_()


class Loader(QtCore.QThread):
    statusFinished = QtCore.pyqtSignal()
    statusUpdateHook = QtCore.pyqtSignal()
    index = 0
    finished_count = 0
    working_list = []
    working_dict = {}
    index_lock = Lock()
    finished_count_lock = Lock()
    workers = []
    ended = False
    
    def __init__(self, lurl: str):
        super().__init__()
        self.working_list.append(lurl)
    
    def progressUpdate(self):
        gl.max = len(self.working_list)
        gl.progress = self.finished_count
        self.statusUpdateHook.emit()
    
    def run(self):
        for i in range(0, 20):
            self.workers.append(threading.Thread(target=worker, args=(self,)))
            self.workers[i].start()
        while self.finished_count < len(self.working_list):
            with self.index_lock:
                with self.finished_count_lock:
                    self.progressUpdate()
            time.sleep(1)
        for work in self.workers:
            gl.end_workers = True
            work.join()
        gl.loaded_dict = self.working_dict
        self.statusFinished.emit()
        with self.index_lock:
            with self.finished_count_lock:
                self.progressUpdate()
        try:
            loaders.remove(self)
        except:
            pass


def toggleSearch():
    if hasattr(ui.tabs.currentWidget(), "searchable") and ui.tabs.currentWidget().searchable:
        if ui.tabs.currentWidget().searching:
            ui.tabs.currentWidget().regularItems()
            ui.tabs.currentWidget().searching = False
        else:
            text, ok = QInputDialog.getText(ui.centralwidget, 'Search', '')
            if ok:
                ui.tabs.currentWidget().searchItems(text)
                ui.tabs.currentWidget().searching = True


def displayDownloads():
    index = 1
    ui.tabs.tabBar().tabButton(0, QTabBar.RightSide).resize(0, 0)
    for name in glob.glob(os.path.join(os.path.dirname(os.path.realpath(__file__)), "downloads", "*.json")):
        addTreeTab(json.load(open(name, "r")))
        ui.tabs.tabBar().tabButton(index, QTabBar.RightSide).resize(0, 0)
        index += 1


if __name__ == "__main__":
    import sys
    if not os.path.isdir(os.path.join(os.path.dirname(os.path.realpath(__file__)), "downloads")):
        os.mkdir(os.path.join(os.path.dirname(os.path.realpath(__file__)), "downloads"))
    gl = g()
    tabs: List[QTreeView] = []
    app = QtWidgets.QApplication(sys.argv)
    application = ApplicationWindow()
    application.setStyleSheet(qdarkgraystyle.load_stylesheet())
    ui = application.ui
    ui.buttonDownload.clicked.connect(download)
    ui.buttonOnlineView.clicked.connect(openViewTab)
    ui.tabs.tabCloseRequested.connect(closeReq)
    ui.dropRevision.addItems(["10", "11/mms"])
    ui.dropRevision.currentIndexChanged.connect(loadReleases)
    ui.shortcut = QShortcut(QKeySequence("Ctrl+F"), ui.centralwidget)
    ui.shortcut.activated.connect(toggleSearch)
    try:
        gl.token = getToken(auth.id, auth.secret)
        loadReleases()
    except requests.exceptions.ConnectionError:
        ui.buttonDownload.setDisabled(True)
        ui.buttonOnlineView.setDisabled(True)
        ui.dropRelease.setDisabled(True)
        ui.dropRevision.setDisabled(True)
        ui.progress.setDisabled(True)
    displayDownloads()
    application.show()
    sys.exit(app.exec_())
